//
//  Detalheviewcontroler.swift
//  Contato
//
//  Created by COTEMIG on 01/09/22.
//

import UIKit
protocol DetalhesViewControlerDelegate {
    func excluirContato(index: Int)
}
class Detalheviewcontroler: UIViewController {

    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var numerolabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var enderecoLabel: UILabel!
    
    public var index: Int?
    public var contato: Contato?
    public var delegate: DetalhesViewControlerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = contato?.nome

        nomeLabel.text = contato?.nome
        numerolabel.text = contato?.telefone
        emailLabel.text = contato?.email
        enderecoLabel.text = contato?.endereco
        
    }
    @IBAction func excluircontato(_ sender: Any) {
        delegate?.excluirContato(index: index!)
        navigationController?.popViewController(animated: true)
    }
    
}
